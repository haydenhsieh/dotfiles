export FZF_DEFAULT_COMMAND="rg --files"
export FZF_DEFAULT_OPTS="--preview-window 'right,60%' --preview 'bat -n --color=always --paging=never {}'"
export FZF_CTRL_T_OPTS="--bind 'ctrl-/:change-preview-window(hidden|)' --bind 'alt-enter:execute(echo \"vim \")+accept'"
export FZF_CTRL_R_OPTS="--preview-window hidden --preview 'bat -n --color=always -P'"
