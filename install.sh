#! /usr/bin/env bash

blacklist="(theano)" #"|(more pkg_name)|..."
default_install_path=${HOME}
old_ext=".orig"
declare -A options=(
    [install]=$default_install_path
    [uninstall]=f
    [verbose]=f)

verbose() {
    if [[ ${options[verbose]} == t ]];then
        return 0
    else
        return 1
    fi
}

print_options() {
    echo "Options:"
    for opt in ${!options[@]};do
        echo "$opt = ${options[$opt]}"
    done | column -t
}

print_usage() {
cat <<EOL
Usage: ${0:script} -[huvi]
    h: this help description
    u: uninstall
    v: verbose
    i PATH: install path(default ${options[install]})
EOL
}

run_hooks() {
    hook_dir="${PWD}/${1}/.hook"
    if [[ ! -a $hook_dir ]] || [[ ! -d $hook_dir ]] ;then
        return 0
    else
        for script in $(ls $hook_dir | sort);do
            echo "Exec $script"
            (exec ${hook_dir}/$script)
        done
    fi
}

install_pkg() {
    pkg=$1
    item_blacklist="(\.hook)"
    for item in $(ls -a $pkg);do
        [[ "$item" == "." ]] || [[ "$item" == ".." ]] || [[ $item =~ $item_blacklist ]] && continue
        src="${PWD}/${pkg}/$item"
        dst="${install_path}/$item"
        verbose && echo "dst=${dst}"
        if [[ ${options[uninstall]} == f ]];then
            verbose && echo "Install $dst"
            if [[ ! -L $dst ]] && [[ -a ${dst}{$old_ext} ]]; then
                echo "${dst}${old_ext} exist but ${dst} is not symbolic!?"
                exit 1
            fi

            #backup original non-symbolic file or directory
            [[ -a $dst ]] && [[ ! -L $dst ]] && mv ${dst} ${dst}${old_ext}

            ln -s $src $dst
        else
            verbose && echo "Uninstall $dst"
            [[ -L ${dst} ]] && rm ${dst} && verbose && echo "  Remove symlink ${dst}"
            [[ -a ${dst}${old_ext} ]] && mv ${dst}${old_ext} ${dst} && verbose && echo "  Recover ${dst}${old_ext} => ${dst}"
        fi
    done
}

main() {
    verbose && echo "main" && print_options
    install_path=${options[install]}
    pkg_list=()
    for name in $(ls $PWD);do
        [[ -d $name ]] && pkg_list[${#pkg_list[@]}]=$name
    done
    verbose && echo "pkg_list=${pkg_list[@]}"
    mkdir -p $install_path
    for pkg in ${pkg_list[@]} ;do
        if [[ ! $pkg =~ $blacklist ]] ;then
            install_pkg $pkg
            [[ ${options[uninstall]} == f ]] && run_hooks $pkg
        fi
    done
}

if [[ $BASH_SOURCE == $0 ]];then
    while getopts "huvfi:" opt;do
        case $opt in
        h)
            print_usage
            exit 0
        ;;
        u)
            options[uninstall]=t
        ;;
        v)
            options[verbose]=t
        ;;
        i)
            options[install]=$OPTARG
        ;;
        ?)
            exit $OPTERR
        ;;
        esac
    done

    main
fi
