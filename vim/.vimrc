"Vundle
"{{{
set nocompatible
set background=dark
filetype off

set runtimepath+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'

"installed plugins
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/nerdcommenter'
Plugin 'airblade/vim-gitgutter'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'KabbAmine/zeavim'
Plugin 'junegunn/fzf.vim'
Plugin 'liuchengxu/vista.vim'
Plugin 'NLKNguyen/papercolor-theme'
Plugin 'yatli/sleigh.vim'

call vundle#end()
filetype plugin indent on
"}}}
"

syntax on
set noswapfile
colorscheme PaperColor

" Configure terminal color in visual selection
"hi Visual term=reverse ctermbg=238 guibg=lightgrey
"hi Search term=reverse ctermbg=238 guibg=lightgrey

" Searching
set hlsearch
set incsearch

" Spacing
set expandtab                   "Always expand tabs to spaces
set shiftwidth=4
set tabstop=4
set softtabstop=4

" Numbering
set number
set numberwidth=4

set showcmd
set wildmenu

set autoindent
set smartindent

set ignorecase
set smartcase "case sensitive if pattern contains uppercase

set scrolloff=4

set laststatus=2  " Always show the status bar

set list listchars=tab:»·,trail:·

set backspace=indent,eol,start "Configure backspace so it acts as it should act

" autoread when changed outside of Vim
set autoread

"Fzf
set rtp+=~/.fzf

command Sudow :w !sudo tee % > /dev/null

" Jump to the last position when reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
    \| exe "normal g'\"" | endif
endif

" Keys
let mapleader=","
" nmap <leader>da :r !LC_TIME=en_US.utf8 date '+\%F \%a'<CR>
"map <F2> :execute 'ts '.expand('<cword>')<CR>
map <F3> :!ctags -R *<CR>
map <F4> :set paste!<BAR>set paste?<CR>
map <F5> :set list!<BAR>set list?<CR>
map <F6> :set hls!<BAR>set hls?<CR>
map <F7> :registers<CR>
map <F8> :marks<CR>
map <F9> :NERDTreeToggle<CR>
map <F10> :Vista!!<CR>
nmap <c-j> :tabnext<CR>
nmap <c-k> :tabprev<CR>
nmap <leader>n :cnext<CR>
nmap <leader>p :cprevious<CR>
nmap <leader>ig :!iconv -f gbk -t utf8 -c % \| less<CR>
nmap <leader>da :exec 'normal i'.substitute(system("LC_TIME=en_US.utf8 date '+\%F \%a'"),"[\n]*$","","")<CR>

map <F2> :execute 'Tags '.expand('<cword>')<CR>
nmap <leader>t :execute 'Tags '.expand('<cword>')<CR>
nmap <leader>j :Jumps<CR>
nmap <leader>r :Rg<CR>

autocmd BufRead,BufNewFile *.sage setfiletype python
autocmd BufRead,BufNewFile *.sinc setfiletype sleigh
autocmd BufRead,BufNewFile *.slaspec setfiletype sleigh

" Let autoread becomes really auto
autocmd CursorHold,CursorHoldI * :checktime

" Language specific tweaks
"Ruby "Javascript
autocmd FileType ruby set et smartindent shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType javascript set et smartindent shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType html set et smartindent shiftwidth=2 tabstop=2 softtabstop=2

" CtrlP
set wildignore=*/node_modules/*

" Plugins conf
" GitGutter plugin
set signcolumn=yes "Replacement of 'let g:gitgutter_sign_column_always = 1'
let g:gitgutter_realtime = 1
let g:gitgutter_eager = 1
set updatetime=150
highlight clear SignColumn

" Airline
let g:airline_theme='dark'
let g:airline#extensions#fzf#enabled = 1
" let g:airline_section_y = 'Dec: %-3b Hex: 0x%2B'

" Fzf
nmap <C-P> :FZF<CR>

" Vista - replacement of tagbar, taglist
" Disable icon if font is missing
" let g:vista#renderer#enable_icon = 0
" No blink
let g:vista_blink=[0,0]
" Search by fzf in vista window
autocmd FileType vista,vista_kind nnoremap <buffer> <silent> / :<c-u>call vista#finder#fzf#Run()<CR>
